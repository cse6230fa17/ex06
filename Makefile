ex06_OBJS = ex06.o
ex06_LIBS = -lcurand

-include Makefile.inc

include ${PETSC_DIR}/lib/petsc/conf/variables

all: ex06

include ${PETSC_DIR}/lib/petsc/conf/rules

ex06: ${ex06_OBJS} chkopts
	-${CLINKER} -o $@ ${ex06_OBJS} ${PETSC_LIB} ${ex06_LIBS}
	-${DSYMUTIL} $@
	${RM} -f $(ex06_OBJS)

